#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Programa cliente que abre un socket a un servidor."""

import socket
import sys

# Cliente UDP simple.
try:
    METHOD = sys.argv[1]
    DATA = sys.argv[2]
except IndexError or ValueError:
    sys.exit("Usage: python3 client.py method receiver@IP:SIPport")
# Dirección IP del servidor.
INFO = DATA.split("@")[1]
SERVER = INFO.split(":")[0]
PORT = int(INFO.split(":")[-1])

# Contenido que vamos a enviar
LINE = METHOD + " sip:" + DATA.split(":")[0] + " SIP/2.0\r\n"
# Creamos el socket, lo configuramos y lo atamos a un servidor/puerto
with socket.socket(socket.AF_INET, socket.SOCK_DGRAM) as my_socket:
    my_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    my_socket.connect((SERVER, PORT))
    print("Enviando: " + LINE)
    my_socket.send(bytes(LINE, 'utf-8') + b'\r\n')
    data = my_socket.recv(1024)

    mensaje = data.decode('utf-8')
    print('Recibido -- ', mensaje)
    L_ACK = "ACK sip:" + DATA.split(":")[0] + " SIP/2.0\r\n"
    ack1 = "SIP/2.0 100 Trying \r\n\r\nSIP/2.0 180 Ringing \r\n\r\n"
    ack = ack1 + "SIP/2.0 200 OK\r\n\r\n"
    if mensaje == ack:
        my_socket.send(bytes(L_ACK, 'utf-8') + b'\r\n')
        data = my_socket.recv(1024)
    print("Terminando socket...")
