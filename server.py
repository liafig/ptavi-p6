#!/usr/bin/python3
# -*- coding: utf-8 -*-
"""Clase (y programa principal) para un servidor en UDP simple."""

import socketserver
import os
import sys


class Handler(socketserver.DatagramRequestHandler):
    """Clase del servidor."""

    def execute(self):
        """Se ejecuta el programa desde la línea de comandos."""
        comando = "mp32rtp -i 127.0.0.1 -p 23032 < " + AUDIO
        print("Vamos a ejecutar ", comando)
        os.system(comando)

    def handle(self):
        """
        Método handle de la clase.

        (todos los requests serán dirigidos por este método).
        """
        line = self.rfile.read()
        print("El cliente nos manda ", line.decode('utf-8'))
        MES = line.decode("utf-8").split()
        ack1 = "SIP/2.0 100 Trying \r\n\r\nSIP/2.0 180 Ringing \r\n\r\n"
        ack = ack1 + "SIP/2.0 200 OK\r\n\r\n"
        if MES[0] == "INVITE":
            self.wfile.write(bytes(ack, 'utf-8'))
        elif MES[0] == "ACK":
            self.execute()
        elif MES[0] == "BYE":
            self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        elif MES[0] != "INVITE" and MES[0] != "ACK" and MES[0] != "BYE":
            self.wfile.write(b"SIP/2.0 405 Method Not Allowed\r\n\r\n")
        else:
            self.wfile.write(b"SIP/2.0 400 Bad Request\r\n\r\n")


if __name__ == "__main__":
    """Creamos servidor y escuchamos."""
    try:
        IP = sys.argv[1]
        PORT = int(sys.argv[2])
        AUDIO = sys.argv[3]
    except IndexError or ValueError:
        sys.exit("Usage: python3 server.py IP port audio_file")
    serv = socketserver.UDPServer(('', PORT), Handler)
    print("Listening...")
    serv.serve_forever()
